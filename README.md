# tkg-air-gapped


## Pre-Airgapped

Download registry.tar and move into airgapped environment

```
#for 1.3.1
curl -o registry.tar https://tkg-install.s3.us-east-2.amazonaws.com/registry.tar

#for 1.4 (might work for 1.3.1 too, have not tried)
curl -o registry.tar https://tkg-install.s3.us-east-2.amazonaws.com/registry-1_4.tar

#for 1.4 FIPS 
curl -o fips-registry.tar https://tkg-install.s3.us-east-2.amazonaws.com/fips-regisrty.tar
```

Make sure you have a registry.  This just pulls the standard docker registry tar.  

```
docker pull registry:2
docker save registry:2 -o registry-image.tar

```
 copy Tanzu-CLI, registry-image.tar and registry.tar into your airgapped env


## Post-Airgapped


Create a cert for your registry

```
#if you use hostname modify the SAN,  subjectAltName=DNS:

export CERT_DIR=<where you want the certs>
export TKG_CUSTOM_IMAGE_REPOSITORY=<IP or hostname of box>

openssl req \
  -x509 \
  -newkey rsa:4096 \
  -sha256 \
  -days 3560 \
  -nodes \
  -keyout $CERT_DIR/registry.key \
  -out $CERT_DIR/registry.crt \
  -subj "/CN=${TKG_CUSTOM_IMAGE_REPOSITORY}" \
  -extensions san \
  -config <( \
    echo '[req]'; \
    echo 'distinguished_name=req'; \
    echo '[san]'; \
    echo "subjectAltName=IP:${TKG_CUSTOM_IMAGE_REPOSITORY}")

```

Untar the registry.tar in a good location to mount it from (this location is need later)

```
tar xf registry.tar

```

Untar and load registry

```
docker load -i registry-image.tar 

```

Run the registry mounting certs and images

```
  CERT_DIR=$1
  REGISTRY_CACHE=<where you untarred the registry.tar>
   
  docker run -d \
  -p 443:5000 \
  --restart=always \
  --name registry \
  -v $CERT_DIR:/certs \
  -v ${REGISTRY_CACHE}/registry:/var/lib/registry \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/registry.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/registry.key \
  registry:2

```

Add your CA certs to localhost if needed

```
 CERT_FILE=$1
 sudo cp $CERT_FILE /usr/local/share/ca-certificates
 sudo update-ca-certificates

```
Now your ready to follow normal docs

```
#if you are on linux you may need to run this
sudo sysctl net/netfilter/nf_conntrack_max=131072


# note that in 1.4 you must set custom registry and CA before installing plugins
tanzu plugin clean
tanzu plugin install --local cli all
tanzu plugin list


# for mac, base64 -b count

export TKG_CUSTOM_IMAGE_REPOSITORY_CA_CERTIFICATE=$(cat $CERT_FILE/registry.crt | base64 -w0)
export TKG_CUSTOM_IMAGE_REPOSITORY=<IP or hostname of box>
#for FIPS
export TKG_CUSTOM_COMPATIBILITY_IMAGE_PATH=fips/tkg-compatibility

#for FIPS (need to pull the BOM file once, then update OVA for vsphere) 
- name: ova-ubuntu-2004
  osinfo:
    name: ubuntu
    version: "20.04"
    arch: amd64
  version: v1.21.2+vmware.1-tkg.1-7832907791984498322

tanzu management-cluster create...

```
